import {Component, EventEmitter, HostListener, NgModule, OnInit, Output} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule, MatSelectModule} from '@angular/material';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})

@NgModule({
  exports: [
  MatButtonModule,
  MatRippleModule,
  MatSelectModule
  ]
})
export class ToolbarComponent implements OnInit {
  button_font_size: number;

  constructor() { }

  ngOnInit() {
    if(window.innerWidth <= 600) {
      this.button_font_size = 35;
    }
    else {
      this.button_font_size = 45;
    }
  }

  returnToTop() {
    window.scrollTo({
      top: 261,
      left: 0,
      behavior: 'smooth'
    });
  }

  @HostListener('window:resize', ['$event'])
  resizeFont(event) {
    if (window.innerWidth <= 600) {
      this.button_font_size = 35;
    } else {
      this.button_font_size = 45;
    }
  }
}
