import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StickyHeaderComponent } from './sticky-header/sticky-header.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatDividerModule, MatGridListModule, MatTabsModule} from '@angular/material';
import { HomePageComponent } from './home-page/home-page.component';
import {RouterModule, Routes} from '@angular/router';
import { AboutMeComponent } from './about-me/about-me.component';
import { FormsModule } from '@angular/forms';
import { ResumeComponent } from './resume/resume.component';
import { ProjectsComponent } from './projects/projects.component';
import { ReferencesComponent } from './references/references.component';
import { HttpClientModule } from '@angular/common/http';
import {PdfViewerModule} from 'ng2-pdf-viewer';


const appRoutes: Routes = [
  {path: '', component: HomePageComponent, data: { animation: 'HomePage' }},
  {path: 'about-me', component: AboutMeComponent, data: {animation:  'AboutMe'}},
  {path: 'resume', component: ResumeComponent, data: {animation: 'Resume'}},
  {path: 'projects', component: ProjectsComponent, data: {animation: 'Projects'}},
  {path: 'references', component: ReferencesComponent, data: {animation: 'References'}}
];

@NgModule({
  declarations: [
    AppComponent,
    StickyHeaderComponent,
    ToolbarComponent,
    HomePageComponent,
    AboutMeComponent,
    ResumeComponent,
    ProjectsComponent,
    ReferencesComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true}
        ),
        MatGridListModule,
        HttpClientModule,
        MatDividerModule,
        PdfViewerModule
    ],
  providers: [],
  exports: [
    StickyHeaderComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
