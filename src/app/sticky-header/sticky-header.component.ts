import {Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sticky-header',
  templateUrl: './sticky-header.component.html',
  styleUrls: ['./sticky-header.component.css']
})
export class StickyHeaderComponent implements OnInit {
  header_height: number = 40;


  constructor() { }

  ngOnInit() {
  }

  resize() {
    if(window.innerWidth <=850){
      this.header_height = 40;
    }
    else{
      this.header_height = 60;
    }
  }
}
