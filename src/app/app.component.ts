import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import {slideInAnimation} from './animations';
import {RouterOutlet} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:[
    trigger('fade',
      [
        state('void', style({ opacity : 0})),
        transition(':enter',[ animate(300)]),
        transition(':leave',[ animate(300)]),
      ]
    ),
    slideInAnimation]
})

export class AppComponent implements OnInit {
  top_padding_var: number = 0;
  header_font_size: number;
  header_height: number;
  header_line_height: number;

  constructor(@Inject(DOCUMENT) document) { }

  ngOnInit() {
    if(window.innerWidth <= 700) {
      this.header_font_size = 45;
      this.header_height = 165;
      this.header_line_height = 8;
    }
    else {
      this.header_font_size = 85;
      this.header_height = 250;
      this.header_line_height = 12;
    }
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 261) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      element.classList.remove('non_sticky');
      if(window.innerWidth <= 850) {
        this.top_padding_var = 93;
      } else {
        this.top_padding_var = 48;
      }
    } else if (window.innerWidth <= 700 && window.pageYOffset > 165) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
      element.classList.remove('non_sticky');
      this.top_padding_var = 84;
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
      element.classList.add('non_sticky');
      this.top_padding_var = 0;
    }
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet &&
      outlet.activatedRouteData &&
      outlet.activatedRouteData['animation'];
  }

  @HostListener('window:resize', ['$event'])
  resizeFont(event) {
    if(window.innerWidth <= 700) {
      this.header_font_size = 45;
      this.header_height = 165;
      this.header_line_height = 8;
    }
    else {
      this.header_font_size = 85;
      this.header_height = 250;
      this.header_line_height = 12;
    }
  }
  //end methods
} // end class
