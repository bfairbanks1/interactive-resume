import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  mobile: boolean;

  constructor() { }

  ngOnInit() {
    if(window.innerWidth <= 900)
      this.mobile = true;
    else this.mobile = false;
  }

  @HostListener('window:resize', ['$event'])
  rearrangeTiles(event) {
    if(window.innerWidth <= 900)
      this.mobile = true;
    else this.mobile = false;
  }
}
