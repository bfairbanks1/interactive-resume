import {Component, NgModule, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Course } from '../course';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule, MatSelectModule} from '@angular/material';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
@NgModule({
  exports: [
    MatButtonModule,
    MatRippleModule,
    MatSelectModule
  ]
})
export class ResumeComponent implements OnInit {
  error: HttpErrorResponse = null;
  daTranscript: Array<Course>;
  sdTranscript: Array<Course>;
  appsection: number;

  constructor(private http: HttpClient) { }

  getTranscript(concentration: string): void {
    this.error = null;
    this.http.get<Array<Course>>('http://localhost:8080/resume/' + concentration)
      .subscribe(response => {
        if(concentration == "data_analytics") {
          this.daTranscript = response;
        }
        else if(concentration == "software_development")
          this.sdTranscript = response;
      },
      (error: HttpErrorResponse) => {
        this.error = error;
      }
    );
  }

  ngOnInit() {
  }

}
